from django.urls import path

from books.views import create_book

urlpatterns = [
    path('create/', create_book),
]