from django.db import models

# Create your models here.
class Book(models.Model):

    book_name = models.CharField(max_length=50)
    book_author = models.CharField(max_length=50)
    page_number = models.IntegerField(max_length=999)
    description = models.TextField(max_length=500)

    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)