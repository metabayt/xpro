from django.shortcuts import render

# Create your views here.
from books.models import Book


def create_book(request):

    if request.method == 'GET':
        return render(request, 'books/create_book.html')

    if request.method == 'POST':
        new_book = Book.objects.create(
            book_name = request.POST['book_name'],
            book_author = request.POST['book_author'],
            page_number = request.POST['page_number'],
            description = request.POST['description']
        )
        new_book.save()
        return render(request, 'books/create_book.html')